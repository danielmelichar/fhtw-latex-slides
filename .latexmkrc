# -*- cperl -*-
# latexmkrc

$recorder = 1;
$pdf_mode = 1;
$bibtex_use = 2;
$force_mode = 1;
$silence_logfile_warnings = 0;
$use_make_for_missing_files = 1;

$bibdir = $ENV{'BIBDIR'};
@BIBINPUTS = ('.',$bibdir);

$target =  $ENV{'TARGET'};
$type = $ENV{'TYPE'};

$builddir = $ENV{'BUILDDIR'};
$dependents_list = 1;
$deps_file = "$builddir/$target.deps";
$out_dir = $builddir;

# $bibtex = 'bibtex %O %B';
$pdflatex = join '', 'pdflatex %O -interaction=nonstopmode -synctex=1 -file-line-error "\PassOptionsToClass{', $type, '}{beamer} \input{%S}"';

push @generated_exts, 'blg', 'cut', 'nav', 'snm', 'vrb', 'synctex.gz';