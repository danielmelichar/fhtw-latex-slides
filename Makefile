BASENAME = demo

BUILDDIR = build
TARGETS  = $(BASENAME)-presentation.pdf \
           $(BASENAME)-handout.pdf      \
           $(BASENAME)-notes.pdf

# Environment
TEXDIRS = resources/latex/:resources/latex-sty/:resources/images/:resources/listings/
BIBDIR  = resources/latex/
BSTDIR  = resources/latex-bst/

.EXPORT_ALL_VARIABLES:
TEXINPUTS := :.:$(TEXDIRS)
# bibtex invoked from BUILDDIR, thus prepend './../'
BSTINPUTS := ./../$(BSTDIR)
BIBINPUTS := :.:./../$(BIBDIR)

# cf. latexmkrc
LATEXMK   = latexmk -deps -jobname="$(basename $1)" $2
LATEXMKF  = export TARGET=$1 && export TYPE=$2 && $(call LATEXMK,$1,$(BASENAME))
LATEXMKC  = export TARGET=$1 && export TYPE="" && $(call LATEXMK,$1,$2)

all: $(TARGETS)

presentation:  $(BUILDDIR)/ $(BASENAME)-presentation.pdf
handout: $(BUILDDIR)/ $(BASENAME)-handout.pdf
notes:   $(BUILDDIR)/ $(BASENAME)-notes.pdf

$(BASENAME)-presentation.pdf: .FORCE | $(BUILDDIR)/
	$(call LATEXMKF,$@,presentation)

$(BASENAME)-handout.pdf: .FORCE | $(BUILDDIR)/
	$(call LATEXMKF,$@,handout)

$(BASENAME)-notes.pdf: .FORCE | $(BUILDDIR)/
	$(call LATEXMKF,$@,notes)

clean:
	$(foreach t,$(TARGETS), $(call LATEXMKC,$(t),-c);)

distclean:
	$(foreach t,$(TARGETS), $(call LATEXMKC,$(t),-C);)

%/:
	mkdir -p $@

.PHONY: all presentation handout notes clean distclean .FORCE

# Disable implicit suffix and built-in rules (for performance and profit)
.SUFFIXES:
MAKEFLAGS += --no-builtin-rules
